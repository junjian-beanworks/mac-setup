#!/usr/bin/env bash

# first backup the .zshrc
cp ~/.zshrc ~/.zshrc.bak

# open .zshrc and seraching THEME, change ZSH_THEME setting to ZSH_THEME="agnoster"

# download powerline
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k

# add new theme setting to  ~/.zshrc ZSH_THEME="powerlevel9k/powerlevel9k"
source ./zshrc

# download the powneline font
git clone https://github.com/powerline/fonts.git ~/Downloads/fonts/

# install font
cd ~/Downloads/fonts && ./install.sh

# clean up
cd ..  && rm -rf ./fonts

# pick the font you want on iterm2
